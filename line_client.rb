require 'dotenv'
Dotenv.load

require 'sinatra'
require 'line/bot'
require 'optparse'
require_relative 'google_image_search_js'

def client
  @client ||= Line::Bot::Client.new { |config|
    config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
    config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
  }
end

def text_message(text)
  {
    type: 'text',
    text: text
  }
end

def image_message(url)
  {
    type: 'image',
    originalContentUrl: url,
    previewImageUrl:    url
  }
end

def option_parser(text)
  text = text.split[1..-1]
  opts = {:position => [0]}
  opt = OptionParser.new do |opt|
    opt.on('-p', '--position Number', Array, 'Change scraping position.') do |num|
      opts[:position] = num.map(&:to_i)
    end
    opt.on('-e', '--except Words', Array, 'Search excpting input words.') do |words|
      opts[:except] = words.map{ |word| word.insert(0, '-') }
    end
    opt.on('-m', '--multi', 'Multi search.') do |bool|
      opts[:multi] = bool
    end
    opt.on_tail("-h", "--help", "Show this message") do |bool|
      opts[:help_message] = bool
    end
  end
  opts[:keyword] = opt.parse!(text)
  return opts
end

def usage
  <<~'EOS'
    Usage: @gis_bot keyword [options]
      -p, --position Number            Change scraping position.
      -e, --except   Words             Search excpting input words.
      -m, --multi                      Multi search.
      -h, --help                       Show this message.
  EOS
end

`echo #{Process.pid} > gis_pid`

post '/callback' do
  body = request.body.read

  signature = request.env['HTTP_X_LINE_SIGNATURE']
  unless client.validate_signature(body, signature)
    error 400 do 'Bad Request' end
  end

  events = client.parse_events_from(body)
  events.each do |event|
    case event
    when Line::Bot::Event::Message
      case event.type
      when Line::Bot::Event::MessageType::Text
        begin
          next unless event.message['text'] && event.message['text'].start_with?('@gis_bot', 'gis_bot', '@gis', 'gis')
          parsed_text = option_parser(event.message['text'])
          (client.reply_message(event['replyToken'], text_message(usage)) and next) if parsed_text[:help_message] || parsed_text[:keyword].empty?
          parsed_text[:position].each do |position|
            params = parsed_text.merge(position: position)
            if parsed_text[:multi]
              parsed_text[:keyword].each do |keyword|
                client.reply_message(event['replyToken'], image_message(GoogleImageSearchJs.new([keyword], params).search))
              end
            else
              client.reply_message(event['replyToken'], image_message(GoogleImageSearchJs.new(params[:keyword], params).search))
            end
          end
        rescue Exception => e
          client.reply_message(event['replyToken'], text_message(usage))
          puts e
          puts e.backtrace
        end
      end
    end
  end

  "OK"
end

