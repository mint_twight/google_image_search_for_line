require 'dotenv'
Dotenv.load

WORKING_DIR = File.expand_path(File.dirname(__FILE__))

God.port = ENV.fetch('GOD_PORT', 17165).to_i
God.watch do |w|
  w.name     = 'gis_line'
  w.dir      = WORKING_DIR
  w.start    = "ruby #{WORKING_DIR}/line_client.rb"
  w.restart  = "kill `cat gis_pid`"
  w.interval = 5.seconds

  w.start_if do |start|
    start.condition(:process_running) do |c|
      c.running = false
    end
  end
end
